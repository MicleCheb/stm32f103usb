#include <stdint.h>
#include <string.h>
#include "Board.h"
#include "CDCACM.h"
#include "xprintf.h"
//  IN  FIFO
struct FIFO_t IN_FIFO;
// OUT FIFO
struct FIFO_t OUT_FIFO;
// usb
struct xCdc_t xCdc;

void putCdc(unsigned char  ch)
{
    while((uint16_t)(IN_FIFO.Put - IN_FIFO.Get) >= DATABUFSIZE);
    IN_FIFO.buf[IN_FIFO.Put++ % DATABUFSIZE] = ch;
}
unsigned char  getCdc(void)
{
    while(OUT_FIFO.Get == OUT_FIFO.Put);
    return OUT_FIFO.buf[OUT_FIFO.Get++ % DATABUFSIZE];
}

////// put byte to USB IN fifo
//void cdcPutChar(uint8_t ch) //
//{
//    uint8_t i = IN_FIFO.Put;
//    ++i;
//    while(i == IN_FIFO.Get);
//    IN_FIFO.buf[IN_FIFO.Put++] = ch;
//}
//// get byte from USB OUT fifo
//uint8_t cdcGetChar(uint8_t *ch) //
//{
//    if(OUT_FIFO.Get == OUT_FIFO.Put) return 0;
//    *ch = OUT_FIFO.buf[OUT_FIFO.Get++];
//    return 1;
//}
//
void usbInit(void)
{
    memset(( uint8_t *)&xCdc,0,sizeof(struct xCdc_t));
    usbHardwareInit();
    OUT_FIFO.Put = 0;
    OUT_FIFO.Get = 0;
    IN_FIFO.Put = 0;
    IN_FIFO.Get = 0;
}
// Setup packet
void ep0Setup(void)
{
    xCdc.pEp0Buf = ( uint8_t* )&xCdc.bReq;
    usbReadControlEp();
    if(xCdc.bEp0Length != sizeof(struct usbSetupPacket_t ))
    {
        return;
    }
    xCdc.bEp0Length = 0;
    xCdc.bEp0State = usbSetupDone;
//    xprintf("SetUp %u ",xCdc.bReq.bRequest);
//    hexprtBuf(( uint8_t* )&xCdc.bReq,sizeof(struct usbSetupPacket_t ));
    switch (xCdc.bReq.bmRequestType & USB_REQ_TYPE_MASK)
    {
//     Standard Requests
    case USB_REQ_TYPE_STANDARD:
        switch (xCdc.bReq.bmRequestType & USB_REQ_TYPE_RECIP_MASK)
        {
        case USB_REQ_TYPE_DEVICE:
            usbStdDeviceReq();
            break;
        case USB_REQ_TYPE_INTERFACE:
            usbStdInterfaceReq();
            break;
        case USB_REQ_TYPE_ENDPOINT:
            usbStdEndPointReq();
            break;
        default:
//            usbStallEp(USB_CONTROL_EP);
            xCdc.bEp0State = usbEp0Stall;
            break;
        }
        break;
// CDC
    case USB_REQ_TYPE_CLASS:
        switch (xCdc.bReq.bRequest)
        {
        case USB_CDC_SEND_ENCAPSULATED_COMMAND:
//            usbWriteControlEp();
            xCdc.bEp0State = usbEp0Stall;
            break;
        case USB_CDC_GET_ENCAPSULATED_COMMAND:
//            usbWriteControlEp();
            xCdc.bEp0State = usbEp0Stall;
            break;
        case USB_CDC_GET_LINE_CODING:
            xCdc.pEp0Buf = (uint8_t *)&uartCDC;
            xCdc.bEp0Length = sizeof(struct LineCoding_t);
//            usbWriteControlEp();
            break;
        case USB_CDC_SET_LINE_CODING:
            xCdc.bEp0State = usbEp0Rx;
            xCdc.pEp0Buf = (uint8_t *)&uartCDC;
            break;
        case USB_CDC_SET_CONTROL_LINE_STATE:        // it's work!
        {
//bit1
//Carrier control for half duplex modems. This signal corresponds to V.24 signal 105 and
//RS232 signal RTS.
//0: Deactivate carrier.
//1: Activate carrier.
//The device ignores the value of this bit when operating in full duplex mode.
//bit0
//Indicates to DCE if DTE is present or not.This signal corresponds to V.24 signal 108/2
//and RS232 signal DTR.
//0: DTE is not present.
//1: DTE is present
            if(xCdc.bReq.wValue&CDC_DTR) cdcSetDtr();
            else cdcCleareDtr();
            if(xCdc.bReq.wValue&CDC_RTS) cdcSetRts();
            else cdcCleareRts();
//            usbWriteControlEp();
            usbCdcNotification();
        }
        break;
        case USB_CDC_SEND_BREAK:
//            usbWriteControlEp();
            break;
        default:
            xCdc.bEp0State = usbEp0Stall;
//            usbStallEp(USB_CONTROL_EP);
            break;
        }
        break;
//
    case USB_REQ_TYPE_VENDOR:
        xCdc.bEp0State = usbEp0Stall;
//        usbStallEp(USB_CONTROL_EP);
        break;
    default:
        xCdc.bEp0State = usbEp0Stall;
//        usbStallEp(USB_CONTROL_EP);
        break;
    }
}
//
void  usbStdDeviceReq(void)
{
    switch (xCdc.bReq.bRequest)
    {
    case USB_REQ_GET_STATUS:
        xCdc.pEp0Buf[0]=0;
        xCdc.pEp0Buf[1]=0;
        xCdc.bEp0Length = 2;
//        usbWriteControlEp();
        break;
    case USB_REQ_SET_FEATURE:
//        usbWriteControlEp();
        break;
    case USB_REQ_CLEAR_FEATURE:
//        usbWriteControlEp();
        break;
    case USB_REQ_SET_ADDRESS:
        xCdc.bEp0State = usbEp0Addr;
//        usbWriteControlEp();
        break;
    case USB_REQ_GET_DESCRIPTOR:
        usbGetDescriptor(&xCdc);
        if(xCdc.bEp0Length > 0)
        {
            xCdc.bEp0State = usbEp0Tx;
//            usbWriteControlEp();
        }
        else
        {
            xCdc.bEp0State = usbEp0Stall;
//            usbStallEp(USB_CONTROL_EP);
        }
        break;
    case USB_REQ_SET_DESCRIPTOR:
        xCdc.bEp0State = usbEp0Stall;
//        usbStallEp(USB_CONTROL_EP);
        break;
    case USB_REQ_GET_CONFIGURATION:
        xCdc.pEp0Buf[0] = xCdc.bConfiguration;
        xCdc.bEp0Length = 1;
//        usbWriteControlEp();
        break;
    case USB_REQ_SET_CONFIGURATION:
        if(xCdc.bReq.wValue == 0)
        {
            RedLedMask = MaskFind;
//            usbWriteControlEp();
//            usbSetAddr(0);
            xCdc.bEp0State = usbNotConfigured;
            xCdc.bConfiguration = 0;
//            usbSetControlInterrupt();
        }
        else  if(xCdc.bReq.wValue == 1) // we have 1 configuration
        {
            RedLedMask = MaskNorma;
            xCdc.bConfiguration = 1;
            usbSetDataInterrupt();
//            usbWriteControlEp();
        }
        else
            xCdc.bEp0State = usbEp0Stall;
//            usbStallEp(USB_CONTROL_EP);
        break;
    default:
        xCdc.bEp0State = usbEp0Stall;
//        usbStallEp(USB_CONTROL_EP);
        break;
    }
}
//
void  usbStdInterfaceReq(void)
{
    switch (xCdc.bReq.bRequest)
    {
    case USB_REQ_GET_STATUS:
        xCdc.pEp0Buf[0]=0;
        xCdc.pEp0Buf[1]=0;
        xCdc.bEp0Length = 2;
//        usbWriteControlEp();
        break;
    case USB_REQ_SET_FEATURE:
//        usbWriteControlEp();
        break;
    case USB_REQ_CLEAR_FEATURE:
//        usbWriteControlEp();
        break;
    case USB_REQ_GET_INTERFACE:
        xCdc.pEp0Buf[0]=xCdc.bInterface;
        xCdc.bEp0Length = 1;
//        usbWriteControlEp();
        break;
    case USB_REQ_SET_INTERFACE:
        xCdc.bInterface = xCdc.bReq.wValue;
//        usbWriteControlEp();
        break;
    default:
        xCdc.bEp0State = usbEp0Stall;
//        usbStallEp(USB_CONTROL_EP);
        break;
    }
}
//
void  usbStdEndPointReq(void)
{
    switch (xCdc.bReq.bRequest)
    {
    case USB_REQ_GET_STATUS:
        xCdc.pEp0Buf[0]=0;
        xCdc.pEp0Buf[1]=0;
        xCdc.bEp0Length = 2;
//        usbWriteControlEp();
        break;
    case USB_REQ_SET_FEATURE:
//        usbWriteControlEp();
        break;
    case USB_REQ_CLEAR_FEATURE:
//        usbWriteControlEp();
        break;
    case USB_REQ_SYNCH_FRAME:
        xCdc.bEp0State = usbEp0Stall;
//        usbStallEp(USB_CONTROL_EP);
        break;
    default:
        xCdc.bEp0State = usbEp0Stall;
//        usbStallEp(USB_CONTROL_EP);
        break;
    }
}
//
// Walk through the list of descriptors and find a match
// return length of descriptorn in usbSetupPacket.wLength
void  usbGetDescriptor( struct xCdc_t* pCdc)
{
    uint8_t type = pCdc->bReq.wValue>>8;
    uint8_t index = pCdc->bReq.wValue&0x00FF;
    uint16_t pos;
    uint8_t len;
    pos=0;
    while (pCdc->pDesc[pos] > 0)
    {
        if ((pCdc->pDesc[pos+1] == type) && (index-- == 0))
        {
            if (type == USB_DESC_CONFIGURATION) len = pCdc->pDesc[pos+2];
            else len = pCdc->pDesc[pos];
            if(pCdc->bReq.wLength > len) pCdc->bReq.wLength = len;
// Found
            pCdc->pEp0Buf = pCdc->pDesc + pos;
            pCdc->bEp0Length = pCdc->bReq.wLength;
            return;
        }
        pos += pCdc->pDesc[pos];
    };
// Not Found
    pCdc->bReq.wLength = 0;
    pCdc->bEp0Length = 0;
}
// CDC EP
void usbCdcNotification(void)
{
    struct cdcNotification_t pkt;
    pkt.bmRequestType = 0xA1;      //   bmRequestType
    pkt.bNotification = 0x20;      //   bNotification
    pkt.wValue = 0;                //   wValue
    pkt.wIndex = 0;                //   wIndex
    pkt.wLength = 2;               //   wLength
    pkt.wStatus = xCdc.wSerialStatus;  //  cdcLineCoding.bError;
    usbWriteCdcEp((uint8_t *)&pkt);
}
