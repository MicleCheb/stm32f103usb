#ifndef CDCDESC_H_INCLUDED
#define CDCDESC_H_INCLUDED
//#include <string.h>
#include <stdint.h>

#define USB_NONE_EP         -1
#define USB_CONTROL_EP      0
#define USB_INT_EP          1
#define USB_CDC_EP          1
#define USB_OUT_EP          2
#define USB_IN_EP           3
#define USB_CONTROL_SIZE    32
#define EP_DOUBLE_BUFFER    0
#define USB_CDC_SIZE        16
#define USB_DATA_SIZE       64
#define USB_IN_SIZE         (USB_DATA_SIZE)
#define USB_OUT_SIZE        (USB_DATA_SIZE)

#define LE(X) ((uint16_t)(X)&0x00FF),((uint16_t)(X)>>8)
const uint8_t cdcDescriptor[]=
{
//Device Descriptor:
//------------------------------
    0x12,       //	bLength
    0x01,       //	bDescriptorType
    LE(0x0200), //	bcdUSB
    0x02,       //	bDeviceClass   (Communication Device Class)
    0x00,       //	bDeviceSubClass 0
    0x00,       //	bDeviceProtocol
    USB_CONTROL_SIZE,   //	bMaxPacketSize0   (64 Bytes)
    LE(0x0483), //	idVendor
    LE(0x7540), //	idProduct LE(0x5740),
    LE(0x0200), //	bcdDevice Revision level of device
    0x01,       //	iManufacturer
    0x02,       //	iProduct
    0x03,       //	iSerialNumber
    0x01,       //	bNumConfigurations

//Configuration Descriptor:
//------------------------------
    0x09,       //	bLength
    0x02,       //	bDescriptorType
    LE(67),     //	wTotalLength
    0x02,       //	bNumInterfaces
    0x01,       //	bConfigurationValue
    0x04,       //	iConfiguration
    0x80,       //	bmAttributes   (Bus-powered Device)
    0x32,       //	bMaxPower   (100 mA)

//Interface Descriptor:
//------------------------------
    0x09,       //	bLength
    0x04,       //	bDescriptorType
    0x00,       //	bInterfaceNumber
    0x00,       //	bAlternateSetting
    0x01,       //	bNumEndPoints
    0x02,       //	bInterfaceClass   (Communication Device Class)
    0x02,       //	bInterfaceSubClass   (Abstract Control Model)
    0x01,       //	bInterfaceProtocol   (Common AT commands)
    0x05,       //	iInterface

//CDC Header Functional Descriptor:
//------------------------------
    0x05,       //	bFunctionalLength
    0x24,       //	bDescriptorType
    0x00,       //	bDescriptorSubtype
    LE(0x0110), //	bcdCDC

//CDC Call Management Functional Descriptor:
//------------------------------
    0x05,       //	bFunctionalLength
    0x24,       //	bDescriptorType
    0x01,       //	bDescriptorSubtype
    0x00,       //  bmCapabilities Device does not handle call management itself.
    0x01,       //	bDataInterface

//CDC Abstract Control Management Functional Descriptor:
//------------------------------
    0x04,       //	bFunctionalLength
    0x24,       //	bDescriptorType
    0x02,       //	bDescriptorSubtype
    0x02,       // bmCapabilities
//  D1: 1 - Device supports the request combination of Set_Line_Coding,
//  Set_Control_Line_State, Get_Line_Coding, and the notification Serial_State.
//    // bitmap for Serial_State notification
//     #define CDC_DCD         0x01
//     #define CDC_DSR         0x02
//     #define CDC_BREAK       0x04    // break reception
//     #define CDC_RI          0x08
//     #define CDC_FRAME       0x10    // frame error
//     #define CDC_PARITY      0x20    // parity error
//     #define CDC_OVRRUN      0x40    // overrun error
//
//     #define CDC_SERIAL_STATE_MASK   (CDC_DCD | CDC_DSR)     // these bits hold their state after notification is sent
//                                                             // other bits are cleared (single shot)
//                                                             // see section "6.3.5 SerialState" on the CDC spec
//     typedef union Serial_State_union
//     {
//         BYTE Val;
//         struct{
//             unsigned DCD         :1;    // bit0:
//             unsigned DSR         :1;    // bit1:
//             unsigned break_in    :1;    // bit2:
//             unsigned RI          :1;    // bit3
//             unsigned frame_err   :1;    // bit4:
//             unsigned parity_err  :1;    // bit5:
//             unsigned overrun_err :1;    // bit6:
//             unsigned reserved    :1;    // bit7:
//         };
//     } SERIAL_STATE_T;
//     void CDC_Serial_State_Service( void )
//     {
//         if(  (cdc_serial_state.Val != cdc_serial_state_hold)    // when any change on the Serial State bitmap
//           && !USBHandleBusy( cdcCommInHandle ) ) {              // and the interrupt IN EP is not occupied,
//                                                                 // fill Serial_State packet
//             cdc_notice.packet[0] = 0xA1;                        //   bmRequestType
//             cdc_notice.packet[1] = SERIAL_STATE;                //   bNotification
//             cdc_notice.packet[2] = 0x00;                        //   wValue
//             cdc_notice.packet[3] = 0x00;
//             cdc_notice.packet[4] = 0x00;                        //   wIndex
//             cdc_notice.packet[5] = 0x00;
//             cdc_notice.packet[6] = 0x02;                        //   wLength
//             cdc_notice.packet[7] = 0x00;
//             cdc_notice.packet[8] = cdc_serial_state.Val;        //   UART state bitmap
//             cdc_notice.packet[9] = 0x00;
//                                                                 // pass the packet to the USB engine
//             cdcCommInHandle = USBTxOnePacket(CDC_COMM_EP, (BYTE*)&cdc_notice.packet, CDC_COMM_IN_EP_SIZE);
//                                                                 // hold just the status bits, drop the error bits
//             cdc_serial_state.Val &= CDC_SERIAL_STATE_MASK;
//                                                                 // update the hold variable
//             cdc_serial_state_hold = cdc_serial_state.Val;
//         }
//     }

//CDC Union Functional Descriptor:
//------------------------------
    0x05,       //	bFunctionalLength
    0x24,       //	bDescriptorType
    0x06,       //	bDescriptorSubtype
    0x00,       //	bControlInterface bMasterInterface -- the controlling intf for the union
    0x01,       //	bSubordinateInterface(0) bSlaveInterface -- the controlled intf for the union

//EndPoint Descriptor for Interrupt endpoint
//------------------------------
    0x07,               //	bLength
    0x05,               //	bDescriptorType
    0x80+USB_INT_EP,    //	bEndpointAddress   (IN Endpoint)
    0x03,               //	bmAttributes	(Transfer: Interrupt / Synch: None / Usage: Data)
    LE(USB_CDC_SIZE),       //	wMaxPacketSize   (64 Bytes)
    0xff,               //	bInterval

// DATA INTERFACE DESCRIPTOR (9 bytes)
//------------------------------
    0x09,       //	bLength
    0x04,       //	bDescriptorType
    0x01,       //	bInterfaceNumber
    0x00,       //	bAlternateSetting
    0x02,       //	bNumEndPoints
    0x0A,       //	bInterfaceClass   (CDC Data)
    0x00,       //	bInterfaceSubClass
    0x00,       //	bInterfaceProtocol
    0x00,       //	iInterface

//Endpoint Descriptor:
//------------------------------
    0x07,           //	bLength
    0x05,           //	bDescriptorType
    0x80+USB_IN_EP, //	bEndpointAddress   (IN Endpoint)
    0x02,           //	bmAttributes	(Transfer: Bulk / Synch: None / Usage: Data)
    LE(USB_IN_SIZE),   //	wMaxPacketSize   (64 Bytes)
    0xff,           //	bInterval: ignored for Bulk transfer

//Endpoint Descriptor:
//------------------------------
    0x07,           //	bLength
    0x05,           //	bDescriptorType
    USB_OUT_EP,     //	bEndpointAddress   (OUT Endpoint)
    0x02,           //	bmAttributes	(Transfer: Bulk / Synch: None / Usage: Data)
    LE(USB_OUT_SIZE),   //	wMaxPacketSize   (64 Bytes)
    0xff,           //	bInterval: ignored for Bulk transfer

    // String index0, language support

    4,        // Length of language descriptor ID
    3,        // LANGID tag
    0x09, 0x04,    // 0x0409 for English


    // String index1, Manufacturer

    38,        // Length of this string descriptor
    3,        // bDescriptorType
    'S',0x00,'T',0x00,'M',0x00,'i',0x00,'c',0x00,'r',0x00,
    'o',0x00,'e',0x00,'l',0x00,'e',0x00,'c',0x00,'t',0x00,
    'r',0x00,'o',0x00,'n',0x00,'i',0x00,'c',0x00,'s',0x00,

    // String index2, Product

    44,        // Length of this string descriptor
    3,        // bDescriptorType
    'S',0x00,'T',0x00,'M',0x00,'3',0x00,'2',0x00,' ',0x00,
    'V',0x00,'i',0x00,'r',0x00,'t',0x00,'u',0x00,'a',0x00,
    'l',0x00,' ',0x00,'C',0x00,'o',0x00,'m',0x00,'P',0x00,
    'o',0x00,'r',0x00,'t',0x00,

    // String index3, Serial Number

    6,          // Length of this string descriptor
    3,          // bDescriptorType
    'S',0,'0',0x00,

    // String index4, Configuration String
    22,         // Length of this string descriptor
    3,          // bDescriptorType
    'C',0x00,'D',0x00,'C',0x00,' ',0x00,'C',0x00,'o',0x00,
    'n',0x00,'f',0x00,'i',0x00,'g',0x00,


    // String index5, Interface String
    28,        // Length of this string descriptor
    3,        // bDescriptorType
    'C',0x00,'D',0x00,'C',0x00,' ',0x00,'I',0x00,'n',0x00,
    't',0x00,'e',0x00,'r',0x00,'f',0x00,'a',0x00,'c',0x00,
    'e',0x00,
    0,
};

#endif // CDCDESC_H_INCLUDED
