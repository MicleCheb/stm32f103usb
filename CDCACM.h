#ifndef CDCACM_H_INCLUDED
#define CDCACM_H_INCLUDED
#include <stdint.h>
#include <stdlib.h>

#define USB_CONTROL_EP      0
//USB related Constant
#define USB_DESC_DEVICE           1
#define USB_DESC_CONFIGURATION    2
#define USB_DESC_STRING           3
#define USB_DESC_INTERFACE        4
#define USB_DESC_ENDPOINT         5
#define USB_DESC_DEVICE_QUALIFIER 6
#define USB_DESC_OTHER_SPEED      7
#define USB_DESC_INTERFACE_POWER  8
//Bit definitions for DEVICE_REQUEST.bmRequestType
//Bit 7:   Data direction
#define USB_REQ_TYPE_OUTPUT     0x00    //0 = Host sending data to device
#define USB_REQ_TYPE_INPUT      0x80    //1 = Device sending data to host

//Bit 6-5: Type
#define USB_REQ_TYPE_MASK       0x60    //Mask value for bits 6-5
#define USB_REQ_TYPE_STANDARD   0x00    //00 = Standard USB request
#define USB_REQ_TYPE_CLASS      0x20    //01 = Class specific
#define USB_REQ_TYPE_VENDOR     0x40    //10 = Vendor specific

//Bit 4-0: Recipient
#define USB_REQ_TYPE_RECIP_MASK 0x1F    //Mask value for bits 4-0
#define USB_REQ_TYPE_DEVICE     0x00    //00000 = Device
#define USB_REQ_TYPE_INTERFACE  0x01    //00001 = Interface
#define USB_REQ_TYPE_ENDPOINT   0x02    //00010 = Endpoint
#define USB_REQ_TYPE_OTHER      0x03    //00011 = Other

//Values for DEVICE_REQUEST.bRequest
//Standard Device Requests
#define USB_REQ_GET_STATUS              0
#define USB_REQ_CLEAR_FEATURE           1
#define USB_REQ_SET_FEATURE             3
#define USB_REQ_SET_ADDRESS             5
#define USB_REQ_GET_DESCRIPTOR          6
#define USB_REQ_SET_DESCRIPTOR          7
#define USB_REQ_GET_CONFIGURATION       8
#define USB_REQ_SET_CONFIGURATION       9
#define USB_REQ_GET_INTERFACE           10
#define USB_REQ_SET_INTERFACE           11
#define USB_REQ_SYNCH_FRAME             12

//CDC CLASS Requests
#define USB_CDC_SET_LINE_CODING                 0x20
#define USB_CDC_GET_LINE_CODING                 0x21
#define USB_CDC_SET_CONTROL_LINE_STATE          0x22
#define USB_CDC_SEND_BREAK                      0x23
#define USB_CDC_SEND_ENCAPSULATED_COMMAND       0x00
#define USB_CDC_GET_ENCAPSULATED_COMMAND        0x01

// constants corresponding to the various serial parameters
#define CDC_DTR			    0x01
#define CDC_RTS			    0x02
#define CDC_1_STOP		    0
#define CDC_1_5_STOP		1
#define CDC_2_STOP		    2
#define CDC_PARITY_NONE		0
#define CDC_PARITY_ODD		1
#define CDC_PARITY_EVEN		2
#define CDC_PARITY_MARK		3
#define CDC_PARITY_SPACE	4
#define CDC_DCD			    0x01
#define CDC_DSR			    0x02
#define CDC_BREAK		    0x04
#define CDC_RI			    0x08
#define CDC_FRAME_ERR		0x10
#define CDC_PARITY_ERR		0x20
#define CDC_OVERRUN_ERR		0x40
// CDC definitions
#define CS_INTERFACE        0x24
#define CS_ENDPOINT         0x25

#define CDC_CHAR_FORMAT_1_STOP_BIT     0
#define CDC_CHAR_FORMAT_1_5_STOP_BIT   1
#define CDC_CHAR_FORMAT_2_STOP_BIT     2

#define CDC_PARITY_TYPE_NONE           0
#define CDC_PARITY_TYPE_ODD            1
#define CDC_PARITY_TYPE_EVEN           2
#define CDC_PARITY_TYPE_MARK           3
#define CDC_PARITY_TYPE_SPACE          4
// USB INTERRUPT
#define USB_RESET       (1<<0)
#define USB_SOF         (1<<1)
#define USB_CONTROL_OUT (1<<2)
#define USB_CONTROL_IN  (1<<3)
#define USB_DATA_IN          (1<<4)
#define USB_DATA_OUT         (1<<5)
//
/*
bmRequestType specifies the direction of data flow, the type of request, and
the recipient.
Bit 7 (Direction) names the direction of data flow for data in the Data stage.
Host to device (OUT) or no Data stage is zero; device to host (IN) is 1.
Bits 6..5 (Request Type) specify whether the request is one of USB�s standard
requests (00), a request defined for a specific USB class (01), or a request
defined by a vendor-specific driver for use with a particular product or products
(10).
Bits 4..0 (Recipient) define whether the request is directed to the device
(00000) or to a specific interface (00001), endpoint (00010), or other element
(00011) in the device.
*/
//80 06 0001 0000 4000
struct usbSetupPacket_t
{
    uint8_t   bmRequestType;
    uint8_t   bRequest;
    uint16_t  wValue;
    uint16_t  wIndex;
    uint16_t  wLength;
};
// Data structure for SET_LINE_CODING class requests and notification
struct LineCoding_t
{
    uint32_t dwDteRate;     // Data terminal rate
    uint8_t  bStopBits;     // Number of stop bits
    uint8_t  bParityType;   // Parity bit type
    uint8_t  bDataBits;     // Number of data bits
};
//
struct cdcNotification_t
{
    uint8_t bmRequestType;
    uint8_t bNotification;
    uint16_t wValue;
    uint16_t wIndex;
    uint16_t wLength;
    uint16_t wStatus;
};
// Where
// dwDTERate	Baudrate in bits per second.
// bCharFormat	Number of stop bits (0 = 1 stop bit, 1 = 1.5 stop bits, 2 = 2 stop bits).
// bParityType	Parity type (0 = None, 1 = Odd, 2 = Even, 3 = Mark, 4 = Space).
// bDataBits	Number of data bits (5, 6, 7, 8, or 16).
// bState Serial State     bit 0: DTR state
//                         bit 1: RTS state
// error status and line states:
// bit 6: bOverRun
// bit 5: bParity
// bit 4: bFraming
// bit 3: bRingSignal
// bit 2: bBreak
// bit 1: bTxCarrier (DSR (Data Set Ready) line state)
// bit 0: bRxCarrier (DCD (Data Carrier Detected) line status)
// Control endpoint 0
#define usbNotConfigured    0
#define usbEp0Setup         1
#define usbEp0Tx            2
#define usbEp0Rx            3
#define usbEp0Addr          4
#define usbSetupDone        5
#define usbEp0Stall         6
// Out ( RX ) endpoint
#define usbOutEmpty         0
#define usbOutFull          1
// In ( TX ) endpoint
#define usbInNoTx           0
#define usbInTx             1
#define usbInZlp            2
struct xCdc_t
{
    struct usbSetupPacket_t bReq ;
    uint16_t wSerialStatus;    // serial line state
    uint8_t *pDesc; // pointer to usb descriptor
    uint8_t *pEp0Buf;   // pointer to CONTROL EP buffer in-out
    uint8_t bEp0Length; //
    uint8_t bEp0State;  // usb state
    uint8_t bConfiguration; // usb configuration
    uint8_t bInterface; // usb interface
    uint8_t bOutState; // out buffer full
    uint8_t bInState; // in buffer full
};
extern struct xCdc_t xCdc;
extern struct FIFO_t IN_FIFO;
// OUT FIFO
extern struct FIFO_t OUT_FIFO;
//
extern uint8_t uartUsbChar;
//
void hexPrt( uint8_t *buf,uint8_t length);


//
void usbHardwareInit(void);
void usbInit(void);
void usbISR(void);
void usb_UP(void);
void usbSetControlInterrupt(void);
void usbSetDataInterrupt(void);
//
void putCdc(unsigned char  ch);
unsigned char  getCdc(void); //
void cdcPutChar(uint8_t ch); //
uint8_t cdcGetChar(uint8_t *ch); //
uint8_t uartGetChar(uint8_t *ch); //
uint8_t uartPutChar(uint8_t ch); //
void SetUart(struct LineCoding_t* cdcLineSet);
void cdcSetDtr(void);
void cdcCleareDtr(void);
void cdcCleareRts(void);
void cdcSetRts(void);
//
void usbWriteControlEp(void);
void usbReadControlEp(void);
void usbWriteInEp(void);
void usbReadOutEp(void);
void usbWriteCdcEp(uint8_t *buf);
void usbCdcNotification(void);
//
void ep0Setup(void);
void usbStdDeviceReq(void);
void usbStdInterfaceReq(void);
void usbStdEndPointReq(void);
void usbGetDescriptor( struct xCdc_t* pCdc);
void usbSetAddr(uint8_t addr);
uint8_t usbGetAddr(void);
//




#endif // CDCACM_H_INCLUDED

