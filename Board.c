#include "Board.h"
#include "CDCACM.h"
#include "xprintf.h"
// struct FIFO_t RX2_FIFO;
#define RX2_FIFO IN_FIFO
struct LineCoding_t uartCDC;
//uint8_t uart2CMD;
uint16_t BoardFlags,LedTimer;
uint8_t RedLedMask,LedMask;
//RTC_IRQHandler
uint32_t BoardTicks,BoardSeconds;
struct RTCTIME_t rtcTIME;

/**
* @brief This function handles System tick timer.
*/
//void SysTick_Handler(void)
//{
//    /* USER CODE BEGIN SysTick_IRQn 0 */
//
//    /* USER CODE END SysTick_IRQn 0 */
////  HAL_IncTick();
////  HAL_SYSTICK_IRQHandler();
//    /* USER CODE BEGIN SysTick_IRQn 1 */
//
//    /* USER CODE END SysTick_IRQn 1 */
//}

/******************************************************************************/
/* STM32F1xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f1xx.s).                    */
/******************************************************************************/
//
//#define CC2511USB
#ifdef CC2511USB
#include "usbCC2511.h"
#endif // CC2511USB

#define STM32USB
#ifdef STM32USB
#include "usbF103.h"
#endif // STM32USB
//  This function handles USB low priority or CAN RX0 interrupts.
void USB_LP_CAN1_RX0_IRQHandler(void)
{
    usbISR();
}
//
void RTC_IRQHandler(void)
{
    RTC->CRL = 0;
//    rtcTicks.t16[0] = RTC->CNTL;
//    rtcTicks.t16[1] = RTC->CNTH;
    ++BoardTicks;
    if((BoardTicks % BOARDFREQ) == 0) ++BoardSeconds;
//    if(rtcAlarm) --rtcAlarm;
    if(LedTimer)
    {
        --LedTimer;
        if(LedTimer == 0) RedLedMask = MaskNorma;
    }
//
    if((BoardTicks % (BOARDFREQ/LEDFREQ)) == 0)
    {
        if(RedLedMask & LedMask) RED_LED_On;
        else RED_LED_Off;
        LedMask <<= 1;
        if(LedMask == 0) LedMask = 1;
//        CheckButton();
    }

////    // send data to Host
////    if(xCdc.bInState != usbInTx) usbWriteInEp(); //
//////// receive date from buffer
////    if(xCdc.bOutState != usbOutEmpty) usbReadOutEp();
}
//

void USART2_IRQHandler(void)
{
    if(USART2->SR & USART_SR_RXNE)  // Данные пришли?
    {
        IN_FIFO.buf[IN_FIFO.Put++ % DATABUFSIZE] = USART2->DR; // кладём в буфер
    }
}
//
void uart2_putc(unsigned char ch)
{
    USART2->DR = ch;
    while(!(USART2->SR & USART_SR_TC));
}
//
unsigned char uart2_getc(void)
{
    while(RX2_FIFO.Put == RX2_FIFO.Get);
    return RX2_FIFO.buf[RX2_FIFO.Get++ % DATABUFSIZE];
}
//
void initBoard(void)
{
    RCC->APB2ENR = RCC_APB2ENR_IOPAEN + 1*RCC_APB2ENR_IOPBEN +
                   0*RCC_APB2ENR_IOPCEN + 0*RCC_APB2ENR_IOPDEN +
                   0*RCC_APB2ENR_USART1EN + RCC_APB2ENR_AFIOEN;
    GPIOA->CRH = (4<<(4*(15-8)))|   /* PA15 */
                 (4<<(4*(14-8)))|   /* PA14 */
                 (4<<(4*(13-8)))|   /* PA13 */
                 (4<<(4*(12-8)))|   /* PA12 USB+ */
                 (4<<(4*(11-8)))|   /* PA11 USB- */
                 (4<<(4*(10-8)))|   /* PA10 */
                 (4<<(4*( 9-8)))|   /* PA9  */
                 /*Configure GPIO pin : LEDSTLINK_Pin */
                 (1<<(4*( 8-8)));   /* PA8 RedLed */
    GPIOA->CRL = (4<<(4*(7)))|      /* PA7 */
                 (4<<(4*(6)))|      /* PA6 */
                 (4<<(4*(5)))|      /* PA5 */
                 (4<<(4*(4)))|      /* PA4 */
                 /**USART2 GPIO Configuration
                         PA2     ------> USART2_TX
                         PA3     ------> USART2_RX
                 */
                 (8<<(4*(3)))|      /* PA3 RX2 */
                 (9<<(4*(2)))|      /* PA2 TX2 */
                 (4<<(4*(1)))|      /* PA1   */
                 (4<<(4*(0)));      /* PA0 VCC/2 */
// PORTB
    GPIOB->CRH = (4<<(4*(15-8)))|   /* PB15 */
                 /*Configure GPIO pins : SWDCLK_Pin SWDIO_Pin */
                 (4<<(4*(14-8)))|   /* PB14 J_TMS */
                 (4<<(4*(13-8)))|   /* PB13 J_TCK */
                 (4<<(4*(12-8)))|   /* PB12 */
                 (4<<(4*(11-8)))|   /* PB11 */
                 (4<<(4*(10-8)))|   /* PB10 */
                 (4<<(4*( 9-8)))|   /* PB9  */
                 (4<<(4*( 8-8)));   /* PB8 */
    GPIOB->CRL = (8<<(4*(7)))|      /* PB7 */
                 (9<<(4*(6)))|      /* PB6 */
                 (4<<(4*(5)))|      /* PB5 */
                 (4<<(4*(4)))|      /* PB4 */
                 (4<<(4*(3)))|      /* PB3 */
                 (4<<(4*(2)))|      /* PB2 */
                 /*Configure GPIO pin : SWDRST_Pin */
                 (4<<(4*(1)))|      /* PB1 J_RST */
                 (4<<(4*(0)));      /* PB0 */
// PORTC
    /** NONE */
//    GPIOC->CRH = (4<<(4*(15-8)))|   /* PC15 */
//                 (4<<(4*(14-8)))|   /* PC14 */
//                 (4<<(4*(13-8)))|   /* PC13 */
//                 (4<<(4*(12-8)))|   /* PC12 */
//                 (4<<(4*(11-8)))|   /* PC11 */
//                 (4<<(4*(10-8)))|   /* PC10 */
//                 (4<<(4*( 9-8)))|   /* PC9 */
//                 (4<<(4*( 8-8)));   /* PC8 */
//    GPIOC->CRL = (4<<(4*(7)))|      /* PC7 */
//                 (4<<(4*(6)))|      /* PC6 */
//                 (4<<(4*(5)))|      /* PC5 */
//                 (4<<(4*(4)))|      /* PC4 */
//                 (4<<(4*(3)))|      /* PC3 */
//                 (4<<(4*(2)))|      /* PC2 */
//                 (4<<(4*(1)))|      /* PC1 */
//                 (4<<(4*(0)));      /* PC0 */
// PORTD
    /** NONE */
//    GPIOD->CRH = (4<<(4*(15-8)))|   /* PD15 */
//                 (4<<(4*(14-8)))|   /* PD14 */
//                 (4<<(4*(13-8)))|   /* PD13 */
//                 (4<<(4*(12-8)))|   /* PD12 */
//                 (4<<(4*(11-8)))|   /* PD11 */
//                 (4<<(4*(10-8)))|   /* PD10 */
//                 (4<<(4*( 9-8)))|   /* PD9  */
//                 (4<<(4*( 8-8)));   /* PD8 */
//    GPIOD->CRL = (4<<(4*(7)))|      /* PD7 */
//                 (4<<(4*(6)))|      /* PD6 */
//                 (4<<(4*(5)))|      /* PD5 */
//                 (4<<(4*(4)))|      /* PD4 */
//                 (4<<(4*(3)))|      /* PD3 */
//                 (4<<(4*(2)))|      /* PD2 */
//                 (4<<(4*(1)))|      /* PD1 */
//                 (4<<(4*(0)));      /* PD0 */
    RED_LED_On;
    LedMask = 1;
    RedLedMask = MaskAll;
    LedTimer = 255;
///
    SystemClock_Config();
///// Init UART1
//    TX1_FIFO.Put=TX1_FIFO.Get=0;
//    RX1_FIFO.Put=RX1_FIFO.Get=0;
///// REMAP USART1 to PORT B for use with STLink
//    AFIO->MAPR = AFIO_MAPR_USART1_REMAP;
    uartCDC.dwDteRate = BAUDRATE;
    uartCDC.bDataBits = 8;
    uartCDC.bParityType = 0;    // None
    uartCDC.bStopBits = 0;      // 1
//    USART1->BRR = SYSTEMFREQ/BAUDRATE;
//    USART1->CR1 = USART_CR1_RE + USART_CR1_TE + 1*USART_CR1_RXNEIE + 0*USART_CR1_TXEIE+0*USART_CR1_UE;
//    USART1->SR = 0;
//    NVIC_EnableIRQ(USART1_IRQn);
///	USART1->CR1 |= (1 << 13);
///    USART1->CR2 = USART_CR2_STOP;
/// Init UART2
    RX2_FIFO.Put = RX2_FIFO.Get = 0;
    RCC->APB1ENR = RCC_APB1ENR_USART2EN+RCC_APB1ENR_BKPEN+RCC_APB1ENR_PWREN+RCC_APB1ENR_USBEN;
    SetUart(&uartCDC);
//    USART2->BRR = UARTFREQ/BAUDRATE;
//    USART2->CR1 = USART_CR1_RE + USART_CR1_TE + 1*USART_CR1_RXNEIE + 0*USART_CR1_TXEIE+0*USART_CR1_UE;
    USART2->SR = 0;
    NVIC_EnableIRQ(USART2_IRQn);

}
void SetUart(struct LineCoding_t* cdcLineSet)
{
    USART2->CR1 = 0;
    USART2->BRR = UARTFREQ/(cdcLineSet->dwDteRate);
    if(cdcLineSet->bStopBits == 1) USART2->CR2 = USART_CR2_STOP_0+USART_CR2_STOP_1; // 1.5 Stop bit
    else if(cdcLineSet->bStopBits == 2) USART2->CR2 = USART_CR2_STOP_1; //  2 Stop bits
    else USART2->CR2 = 0;   // 1 Stop bit
    if(cdcLineSet->bParityType == 1) USART2->CR1 = USART_CR1_RE + USART_CR1_TE + USART_CR1_RXNEIE + USART_CR1_UE +  \
                USART_CR1_PCE + USART_CR1_PS + USART_CR1_M; // 1 = Odd, 2 = Even,0 - none
    else if(cdcLineSet->bParityType == 2) USART2->CR1 = USART_CR1_RE + USART_CR1_TE + USART_CR1_RXNEIE + \
                USART_CR1_UE + USART_CR1_PCE + USART_CR1_M;
    else USART2->CR1 = USART_CR1_RE + USART_CR1_TE + USART_CR1_RXNEIE + USART_CR1_UE;
}
void cdcSetDtr(void)
{
}
void cdcCleareDtr(void)
{
}
void cdcCleareRts(void)
{
}
void cdcSetRts(void)
{
}
//
void SystemClock_Config(void)
{
    /** Configure system clock generator */
/// После программирования надо сделать RESET или снять питание
    RCC->CR = RCC_CR_HSEON | RCC_CR_HSION; /** Enable HSE */
    while(!(RCC->CR & RCC_CR_HSERDY));
    /** PLL configuration: PLLCLK = HSE/1 * 3 = 24 MHz */
    RCC->CFGR = RCC_CFGR_PLLMULL6 + RCC_CFGR_PLLSRC + RCC_CFGR_SW_PLL + RCC_CFGR_USBPRE + RCC_CFGR_HPRE_DIV2 + RCC_CFGR_ADCPRE_DIV8 + RCC_CFGR_PPRE1_DIV1;
//    RCC->CFGR2 = RCC_CFGR2_PREDIV1_DIV1;
    RCC->CR |= RCC_CR_PLLON; /** Enable PLL */
    while(!(RCC->CR & RCC_CR_PLLRDY)); /** Wait PLL */
    while(!(RCC->CFGR & RCC_CFGR_SWS_PLL));/** Wait SystemClock Switch */
/// RTC config
    RCC->APB1ENR |= RCC_APB1ENR_PWREN + RCC_APB1ENR_BKPEN; // Enable PWR and BKP clocks
    PWR->CR |= PWR_CR_DBP;  // Allow access to BKP Domain
    RCC->BDCR = RCC_BDCR_BDRST;
    RCC->BDCR = RCC_BDCR_RTCSEL_HSE + RCC_BDCR_RTCEN; // select clock
//    while((RCC->BDCR & RCC_BDCR_LSERDY) == 0);
    RTC->CRL = 0;
    while (!(RTC->CRL & RTC_CRL_RTOFF));	/* Wait for RTC internal process */
    RTC->CRL = RTC_CRL_CNF;					/* Enter RTC configuration mode */
    RTC->PRLH = 0;
    RTC->PRLL = RTCFREQ/BOARDFREQ - 1;	    /* Set RTC clock  (HSE div 128) divider for 1/64 sec tick */
    RTC->CRH = RTC_CRH_SECIE;
    RTC->CNTH = 0;
    RTC->CNTL = 0;
    BoardTicks = 0;
    RTC->CRL = 0;						    /* Exit RTC configuration mode */
    while (!(RTC->CRL & RTC_CRL_RTOFF));	/* Wait for RTC internal process */
    while (!(RTC->CRL & RTC_CRL_RSF));      /* Wait for RTC is in sync */
//    PWR->CR &= ~PWR_CR_DBP;	/* (HSE div 128) Inhibit write access to backup domain LSE + LSI */
    BoardTicks = 0;
    rtcTIME.year = 2018;
    rtcTIME.month = 1;
    rtcTIME.mday = 26;
    rtcTIME.wday = 5;
    rtcTIME.hour = 10;
    rtcTIME.min = 5;
    rtcTIME.sec = 23;
    rtc_settime(&rtcTIME);
    NVIC_EnableIRQ(RTC_IRQn);
}
//
void SystemInit (void)
{
    /*!< Vector Table base offset field. This value must be a multiple of 0x200. */
#define VECT_TAB_OFFSET  0x0
/// SCB->VTOR = SRAM_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal SRAM. */
    SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH. */
}
/*------------------------------------------*/
/* Get time in calendar form                */
/*------------------------------------------*/
const uint8_t samurai[] = {31,28,31,30,31,30,31,31,30,31,30,31};
void rtc_gettime (struct RTCTIME_t* rtc)
{
    uint32_t utc, n, i, d;

//    if (!rtc_getutc(&utc)) return 0;
    utc = BoardSeconds;
    utc += (uint32_t)(_RTC_TDIF * 3600);

    rtc->sec = (uint8_t)(utc % 60);
    utc /= 60;
    rtc->min = (uint8_t)(utc % 60);
    utc /= 60;
    rtc->hour = (uint8_t)(utc % 24);
    utc /= 24;
    rtc->wday = (uint8_t)((utc + 4) % 7);
    rtc->year = (uint16_t)(1970 + utc / 1461 * 4);
    utc %= 1461;
    n = ((utc >= 1096) ? utc - 1 : utc) / 365;
    rtc->year += n;
    utc -= n * 365 + (n > 2 ? 1 : 0);
    for (i = 0; i < 12; i++)
    {
        d = samurai[i];
        if (i == 1 && n == 2) d++;
        if (utc < d) break;
        utc -= d;
    }
    rtc->month = (uint8_t)(1 + i);
    rtc->mday = (uint8_t)(1 + utc);
}

/*------------------------------------------*/
/* Set time in calendar form                */
/*------------------------------------------*/

void rtc_settime (struct  RTCTIME_t* rtc)
{
    uint32_t utc, i, y;
    y = rtc->year - 1970;
    if (y > 2106 || !rtc->month || !rtc->mday) return;
    utc = y / 4 * 1461;
    y %= 4;
    utc += y * 365 + (y > 2 ? 1 : 0);
    for (i = 0; i < 12 && i + 1 < rtc->month; i++)
    {
        utc += samurai[i];
        if (i == 1 && y == 2) utc++;
    }
    utc += rtc->mday - 1;
    utc *= 86400;
    utc += rtc->hour * 3600 + rtc->min * 60 + rtc->sec;
    utc -= (uint32_t)(_RTC_TDIF * 3600);
    BoardSeconds = utc;
}
//
void outCalendar(void)
{
    xprintf("Total : %lu\n",BoardTicks/BOARDFREQ); // BoardSeconds
    rtc_gettime (&rtcTIME);
    xprintf("%u-%u-%u %u %u:%u:%u\n", \
            rtcTIME.year,rtcTIME.month,rtcTIME.mday,rtcTIME.wday, \
            rtcTIME.hour,rtcTIME.min,rtcTIME.sec);

}
//
void delay (uint16_t ticks)
{
    uint16_t j = RTC->DIVL;
    while(ticks--)
    {
        while(j == RTC->DIVL);
        j= RTC->DIVL;
    }
}
//
void printUart(void)
{
    /**USART2 GPIO Configuration
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX
    */
    xprintf("\nUART2 PA2(TX) PA3(RX)>start :");
    xprintf(" %lu",uartCDC.dwDteRate);
    xprintf(" %u",uartCDC.bStopBits);
    xprintf(" %u",uartCDC.bParityType);
    xprintf(" %u",uartCDC.bDataBits);
//    xprintf(" %02X\n",uartState);
}
//
uint8_t getDigit(long *digit)
{
    char Addr[64],*pAddr = Addr;
//    long adr;
    xgets(Addr,sizeof(Addr));
    return xatoi(&pAddr, digit);
}
//
