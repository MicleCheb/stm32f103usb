#ifndef USBF103_H_INCLUDED
#define USBF103_H_INCLUDED
#include "STM32F103/stm32f103xb.h"
#include "cdcDescTI.h"
//#include "cdcDescSTM32.h"

void prtUart(void);

#define BTABLE_ADDRESS      (0x00)
// Control EP0
#define ENDP0_RXADDR        (BTABLE_ADDRESS + 8*8)
#define ENDP0_TXADDR        (ENDP0_RXADDR + USB_CONTROL_SIZE)
// CDC EP1
#define ENDP1_TXADDR        (ENDP0_TXADDR + USB_CONTROL_SIZE)
// Data Out EP2
#define ENDP2_RXADDR        (ENDP1_TXADDR + USB_CDC_SIZE)
// Data In EP3
#define ENDP3_TXADDR        (ENDP2_RXADDR + USB_DATA_SIZE)

#define pEpBuf(pBufAddr)   ((uint16_t *)(USB_PMAADDR + 2*(USB->BTABLE + pBufAddr)    ))
#define pEpTxAddr(bEpNum)  ((uint16_t *)(USB_PMAADDR + 2*(USB->BTABLE + bEpNum*8 + 0)))
#define pEpTxCount(bEpNum) ((uint16_t *)(USB_PMAADDR + 2*(USB->BTABLE + bEpNum*8 + 2)))
#define pEpRxAddr(bEpNum)  ((uint16_t *)(USB_PMAADDR + 2*(USB->BTABLE + bEpNum*8 + 4)))
#define pEpRxCount(bEpNum) ((uint16_t *)(USB_PMAADDR + 2*(USB->BTABLE + bEpNum*8 + 6)))

#define EP_VALID_MASK       (0x000F + (15<<8) + (1<<7) + (1<<15))      // non toggle bits
#define EP_CLR_RX_MASK      (0x000F + (15<<8) + (1<<7))  // save addr kind type TX setup
#define EP_CLR_TX_MASK      (0x000F + (15<<8) + (1<<15))  // save addr kind type RX setup
#define EP_CLR_RX_TX_MASK      (0x000F + (15<<8))

void usb_UP(void)
{
    USB->DADDR  = USB_DADDR_EF | 0;
}
void usb_DOWN(void)
{
    USB->DADDR  = 0;
}
//
void usbHardwareInit(void)
{
// Clock already configured in Board.c
    USB->CNTR = USB_CNTR_FRES;
    USB->CNTR = 0;
    USB->ISTR = 0;
    NVIC_EnableIRQ(USB_LP_CAN1_RX0_IRQn);
    USB->CNTR = USB_CNTR_RESETM;
//
    USB->BTABLE = BTABLE_ADDRESS;
//
    USB->DADDR  = USB_DADDR_EF | 0;
}
//uint8_t usbGetInterrupt(void)
void usbISR(void)
{
    uint16_t iFl,epReg;
//
    iFl = USB->ISTR;
    USB->ISTR &= ~0x7F00;  //USB_ISTR_CTR;
    if(iFl & USB_ISTR_RESET) // USB RESET
    {
        xCdc.bEp0State = usbEp0Setup;
        usbSetControlInterrupt();
        return;
    }
//  Correct Transfer Interrupt
    if(iFl & USB_ISTR_CTR)
    {
        switch(iFl & USB_ISTR_EP_ID_Msk)
        {
        case USB_CONTROL_EP:
            epReg = USB->EP0R;
            if((iFl & USB_ISTR_DIR) == 0) // DIR bit=0, CTR_TX bit is set in the USB_EPnR
            {
                USB->EP0R = epReg & EP_CLR_TX_MASK;
//
                if(xCdc.bEp0State == usbEp0Tx)
                {
                    usbWriteControlEp();
                    return;
                }
                if(xCdc.bEp0State == usbNotConfigured)
                {
                    usbSetControlInterrupt();
                }
                if(xCdc.bEp0State == usbEp0Addr)
                {
                    usbSetAddr(xCdc.bReq.wValue);
                }
//
                xCdc.bEp0State = usbEp0Setup;
                USB->EP0R = (USB->EP0R & (EP_VALID_MASK + USB_EP_RX_VALID)) ^ USB_EP_RX_VALID;
            }
            else // DIR bit=1, CTR_RX bit or both CTR_TX/CTR_RX are set in the USB_EPnR register
            {
                USB->EP0R = epReg & EP_CLR_RX_MASK;
//
                if(epReg & USB_EP_SETUP)
                {
                    ep0Setup();
                    if((xCdc.bEp0State == usbSetupDone) || (xCdc.bEp0State == usbEp0Tx) || (xCdc.bEp0State == usbEp0Addr))
                    {
                        usbWriteControlEp();
                        return;
                    }
                    if(xCdc.bEp0State == usbEp0Stall) xCdc.bEp0State = usbEp0Setup;
                    USB->EP0R = (USB->EP0R & (EP_VALID_MASK + USB_EP_RX_VALID)) ^ USB_EP_RX_VALID;
                }
                else if(epReg & USB_EP_CTR_RX)
                {
                    usbReadControlEp();
                    if(xCdc.bEp0State == usbEp0Rx)
                    {
                        if(xCdc.bReq.bRequest == USB_CDC_SET_LINE_CODING)
                        {
                            SetUart(&uartCDC);
                        }
                    }
                    xCdc.bEp0Length = 0;
                    usbWriteControlEp();
                    xCdc.bEp0State = usbSetupDone;
                }
            }
            break;
        case USB_CDC_EP:
            USB->EP1R &= EP_CLR_RX_TX_MASK;
            break;
        case USB_OUT_EP:
            USB->EP2R &= EP_CLR_RX_TX_MASK;
            usbReadOutEp();
            LedTimer = 2*BOARDFREQ;
            RedLedMask = MaskUART;
            break;
        case USB_IN_EP:
            USB->EP3R &= EP_CLR_RX_TX_MASK;
            xCdc.bInState = usbInNoTx;
            usbWriteInEp();
            LedTimer = 2*BOARDFREQ;
            RedLedMask = MaskUART;
            break;
        }
    }
// Start Of Frame Interrupt
// send data to Host
    if(xCdc.bInState != usbInTx) usbWriteInEp(); //
// receive date from buffer
    if(xCdc.bOutState != usbOutEmpty) usbReadOutEp();
//
}
void  usbSetDataInterrupt(void)
{
    USB->CNTR = USB_CNTR_CTRM + 1*USB_CNTR_SOFM + USB_CNTR_RESETM;
/// CDC interrupt EP
    *pEpTxAddr(USB_CDC_EP) = ENDP1_TXADDR;
    USB->EP1R = ((USB_EP_INTERRUPT + 1) & (EP_VALID_MASK + USB_EP_TX_VALID)) ^ USB_EP_TX_NAK;
/// Set the OUT max packet size
    *pEpRxAddr(USB_OUT_EP) = ENDP2_RXADDR;
    if(USB_OUT_SIZE < 62)
        *pEpRxCount(USB_OUT_SIZE) = ((USB_OUT_SIZE/2)<<USB_COUNT0_RX_NUM_BLOCK_Pos);
    else
        *pEpRxCount(USB_OUT_SIZE) = USB_COUNT0_RX_BLSIZE + ((USB_OUT_SIZE/32 -1)<<USB_COUNT0_RX_NUM_BLOCK_Pos);
    USB->EP2R = ((USB_EP_BULK + 2) & (EP_VALID_MASK + USB_EP_RX_VALID)) ^ USB_EP_RX_VALID;
    xCdc.bOutState = usbOutEmpty;
/// Set the IN max packet size, no double buffered
    *pEpTxAddr(USB_IN_EP) = ENDP3_TXADDR;
    USB->EP3R = ((USB_EP_BULK + 3) & (EP_VALID_MASK + USB_EP_TX_VALID)) ^ USB_EP_TX_NAK;
    xCdc.bInState = usbInNoTx;
}
void  usbSetControlInterrupt(void)
{
// Bring up the USB link
    xCdc.pDesc = ( uint8_t*)&cdcDescriptor;
    if(USB_CONTROL_SIZE < 62)
        *pEpRxCount(USB_CONTROL_EP) = ((USB_CONTROL_SIZE/2)<<USB_COUNT0_RX_NUM_BLOCK_Pos);
    else
        *pEpRxCount(USB_CONTROL_EP) = USB_COUNT0_RX_BLSIZE + ((USB_CONTROL_SIZE/32 -1)<<USB_COUNT0_RX_NUM_BLOCK_Pos);
    *pEpRxAddr(USB_CONTROL_EP) = ENDP0_RXADDR;
    *pEpTxAddr(USB_CONTROL_EP) = ENDP0_TXADDR;
//
    USB->CNTR = USB_CNTR_CTRM + 0*USB_CNTR_SOFM + USB_CNTR_RESETM;
    USB->EP0R = USB_EP_CONTROL + USB_EP_RX_VALID + USB_EP_TX_NAK;
    USB->EP0R = (USB->EP0R & (EP_VALID_MASK+USB_EP_RX_VALID)) ^ USB_EP_RX_VALID;
    USB->DADDR = USB_DADDR_EF | 0;
}
void usbSetAddr(uint8_t addr)
{
    USB->DADDR = USB_DADDR_EF|addr;
}
uint8_t usbGetAddr(void)
{
    return USB->DADDR & ~USB_DADDR_EF;
}
void usbWriteControlEp(void)
{
    uint16_t *pmaBuf = pEpBuf(ENDP0_TXADDR);
    uint16_t *cntTx  = pEpTxCount(USB_CONTROL_EP);
    union
    {
        uint16_t d16;
        uint8_t d8[2];
    } v;
    uint8_t cnt,total;
    cnt = xCdc.bEp0Length;
    if(cnt < USB_CONTROL_SIZE)
    {
        if(xCdc.bEp0State != usbEp0Addr)
            xCdc.bEp0State = usbSetupDone;
    }
    if(cnt > USB_CONTROL_SIZE) cnt = USB_CONTROL_SIZE;
    xCdc.bEp0Length -= cnt;
    total = cnt;
    while(cnt > 1)
    {
        v.d8[0] = *xCdc.pEp0Buf++;
        v.d8[1] = *xCdc.pEp0Buf++;
        *pmaBuf = v.d16;
        pmaBuf += 2; // 16 bit to 32bit addr
        cnt -= 2;
    }
    if(cnt) // last odd byte
    {
        v.d8[0] = *xCdc.pEp0Buf;
        v.d8[1] = 0;
        *pmaBuf = v.d16;
    }
    *cntTx = total;
    USB->EP0R = (USB->EP0R  & (EP_VALID_MASK + USB_EP_TX_VALID)) ^ USB_EP_TX_VALID;
}
void usbReadControlEp(void)
{
    uint16_t *pmaBuf = pEpBuf(ENDP0_RXADDR);
    uint16_t *cntRx  = pEpRxCount(USB_CONTROL_EP);
    union
    {
        uint16_t d16;
        uint8_t d8[2];
    } v;
    uint8_t cnt;
    cnt = *cntRx & 0x03FF;
    xCdc.bEp0Length = cnt;
    while(cnt > 1)
    {
        v.d16 = *pmaBuf;
        *xCdc.pEp0Buf++ = v.d8[0];
        *xCdc.pEp0Buf++ = v.d8[1];
        pmaBuf += 2; // 16 bit to 32bit addr
        cnt -= 2;
    }
    if(cnt) // last odd byte
    {
        v.d16 = *pmaBuf;
        *xCdc.pEp0Buf = v.d8[0];
    }
//    USB->EP0R = (USB->EP0R & (EP_VALID_MASK + USB_EP_RX_VALID)) ^ USB_EP_RX_VALID;
}
void usbWriteInEp(void)
{
    uint16_t *pmaBuf = pEpBuf(ENDP3_TXADDR);
    uint16_t *cntTx  = pEpTxCount(USB_IN_EP);
    union
    {
        uint16_t d16;
        uint8_t d8[2];
    } v;
    uint16_t txCnt,total;
    total = IN_FIFO.Put - IN_FIFO.Get;
    if(total != 0)
    {
        if(total >  USB_IN_SIZE) total = USB_IN_SIZE;
        txCnt = total;
        while(txCnt > 1)
        {
            v.d8[0] = IN_FIFO.buf[IN_FIFO.Get++ % DATABUFSIZE];
            v.d8[1] = IN_FIFO.buf[IN_FIFO.Get++ % DATABUFSIZE];
            *pmaBuf = v.d16;
            pmaBuf += 2; // 16 bit to 32bit addr
            txCnt -= 2;
        }
        if(txCnt) // last odd byte
        {
            v.d8[0] = IN_FIFO.buf[IN_FIFO.Get++ % DATABUFSIZE];
            v.d8[1] = 0;
            *pmaBuf = v.d16;
        }
        *cntTx = total;
        xCdc.bInState = usbInTx;
        USB->EP3R = (USB->EP3R & (EP_VALID_MASK + USB_EP_TX_VALID)) ^ USB_EP_TX_VALID;
    }
}
void usbReadOutEp(void)
{
    uint16_t *pmaBuf = pEpBuf(ENDP2_RXADDR);
    uint16_t *cntRx  = pEpRxCount(USB_OUT_EP);
    uint16_t rxCnt = *cntRx & 0x03FF;
    union
    {
        uint16_t d16;
        uint8_t d8[2];
    } v;
    uint16_t freeCnt = DATABUFSIZE - (OUT_FIFO.Put - OUT_FIFO.Get);
    if(freeCnt <  rxCnt) xCdc.bOutState = usbOutFull;
    else
    {
        while(rxCnt > 1)
        {
            v.d16 = *pmaBuf;
            OUT_FIFO.buf[OUT_FIFO.Put++ % DATABUFSIZE] = v.d8[0];
            OUT_FIFO.buf[OUT_FIFO.Put++ % DATABUFSIZE] = v.d8[1];
            pmaBuf += 2; // 16 bit to 32bit addr
            rxCnt -= 2;
        }
        if(rxCnt) // last odd byte
        {
            v.d16 = *pmaBuf;
            OUT_FIFO.buf[OUT_FIFO.Put++ % DATABUFSIZE] = v.d8[0];
        }
        xCdc.bOutState = usbOutEmpty;
//        *cntRx &= ~0x03FF;
        USB->EP2R = (USB->EP2R & (EP_VALID_MASK + USB_EP_RX_VALID)) ^ USB_EP_RX_VALID;
    }
}
void usbWriteCdcEp(uint8_t *buf)
{
    uint16_t *pmaBuf = pEpBuf(ENDP1_TXADDR);
    uint16_t *cntTx  = pEpTxCount(USB_CDC_EP);
    union
    {
        uint16_t d16;
        uint8_t d8[2];
    } v;
    uint8_t cnt = sizeof(struct cdcNotification_t),total;
    total = cnt;
    while(cnt)
    {
        v.d8[0] = *buf++;
        v.d8[1] = *buf++;
        *pmaBuf = v.d16;
        pmaBuf += 2; // 16 bit to 32bit addr
        cnt -= 2;
    }
    *cntTx = total;
    USB->EP1R = (USB->EP1R & (EP_VALID_MASK + USB_EP_TX_VALID)) ^ USB_EP_TX_VALID;
}
#endif // USBF103_H_INCLUDED
