/* Flasher from  - STM32F103 to STM32F100 */
#include "Board.h"
#include "CDCACM.h"
#include "xprintf.h"
void prtUart(void);
void prtUSB(void);
void hexPrt( uint8_t *buf,uint8_t length);
uint8_t uartUsbChar;
int main(void)
{
    initBoard();
///    xdev_out(uart2_putc);
///    xdev_in(uart2_getc);
    USART2_ENABLE;
    usbInit();
    RED_LED_Off;
    RedLedMask = MaskFind;
    while(BoardTicks < 5*BOARDFREQ);
    xdev_out(putCdc);
    xdev_in(getCdc);
    xputs("STM32F103 UART to USB bridge\n");
    while(1)
    {
/// OUT FIFO not empty Send to TX2
        if(OUT_FIFO.Put != OUT_FIFO.Get)
        {
            if(USART2_EMPTY)
            {
                uartUsbChar = OUT_FIFO.buf[OUT_FIFO.Get++ % DATABUFSIZE];
                if(uartUsbChar == 0x04)
                {
                    xprintf("STM32F103 USB to UART bridge\n");
                    prtUart();
                    outCalendar();
                }
                else USART2_SEND(uartUsbChar);
            }
        }
/// RX2 not empty served in usb interrupt handler
///        __WFI(); //
    }
    //  Never return here
    return 0;
}
//
//void printHelp(void)
//{
//    xprintf("\nSTM32 USB Flash Loader\n");
//    xprintf(" H - Help\n");
//    xprintf(" U - USB-RS232 (Ctrl-Q end)\n");
//    xprintf(" P - MCU FLASH LOADER\n");
//    xprintf(" R - USB-RADIO (Ctrl-Q end)\n");
//    xprintf(" T - DS18B20 (Q end)\n");
////    printf(" S - USB-SPI (Ctrl-Q end)\n");
//    xprintf(" B - Self BootLoader\n");
////    xprintf(" Q - REBOOT\n");
//}


//void prtUSB(void)
//{
////    printf("USB %u %u\n",USBADDR,xCdc.bConfiguration);
////printf("%u %u %u %u\n",USBADDR,xCdc.bConfiguration);
//
//}
void prtUart(void)
{
    xprintf("Line Setting : %lu", uartCDC.dwDteRate);
    xprintf(",%u,%u,%u\n",uartCDC.bStopBits,uartCDC.bParityType,uartCDC.bDataBits);
}
//
void hexPrt( uint8_t *buf,uint8_t length)
{
    while(length--) xprintf("%02x",*buf++);
    xprintf("\n");
}
