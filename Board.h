#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED
#include <stdint.h>
#include <string.h>

#include "STM32F103/stm32f103xb.h"

#define sBit(byte,bit)     ( (byte) |= (1<< (bit) ))
#define cBit(byte,bit)     ( (byte) &=~(1<< (bit) ))
#define fBit(byte,bit)     ( (byte) ^= (1<< (bit) ))
#define tBit(byte,bit)     ( (byte) &  (1<< (bit) ))
//
#define HSE_VALUE    ((uint32_t)8000000)
#define HSI_VALUE    ((uint32_t)8000000)
#define SYSTEMFREQ   ((uint32_t)24000000*1)
#define UARTFREQ     ((uint32_t)24000000*1)
#define RTCFREQ      (HSE_VALUE/128)
#define BOARDFREQ    ( 8 )
#define BOARDSEC     ( BOARDFREQ )
#define LEDFREQ      ( 8 )
#define BUTTONFREQ   ( 2*LEDFREQ )
#define BAUDRATE     ( 115200 )

#define flT         (1<<10)  // Ticks Timer
#define flButton    (1<<9)
#define flUart      (1<<8)

/**BUTTON GPIO Configuration
    PA0     ------> BUTTON
*/
#define BUTTON_IS_PRESSED      GPIOA->IDR&(1<<0)
/**LED GPIO Configuration
    PC8     ------> BLUE_LED
    PC9     ------> GREEN_LED
*/
#define RED_LED_On         GPIOA->BSRR=(1<<8)
#define RED_LED_Off        GPIOA->BSRR=(1<<(8+16))
#define RED_LED_Flp        GPIOA->ODR^=(1<<8)

#define BLUE_LED_On         GPIOC->BSRR=(1<<8)
#define BLUE_LED_Off        GPIOC->BSRR=(1<<(8+16))
#define BLUE_LED_Flp        GPIOC->ODR^=(1<<8)
#define GREEN_LED_On        GPIOC->BSRR=(1<<9)
#define GREEN_LED_Off       GPIOC->BSRR=(1<<(9+16))
#define GREEN_LED_Flp       GPIOC->ODR^=(1<<9)
/// LED MASK
#define MaskOff     0x00
#define MaskFind    0x27
#define MaskProg    0x23
#define MaskNorma   0x01
#define MaskUART    0xAA
#define MaskAll     0x77
/// FLASHER
/// Reset
#define RST_PORT            GPIOA
#define RST_PIN             8
#define RST_INPUT           RST_PORT->CRH|=(4<<(4*(RST_PIN-8)))
#define RST_OUTPUT          RST_PORT->CRH&=~(6<<(4*(RST_PIN-8)))
#define RST_HIGH            RST_PORT->BSRR=(1<<RST_PIN)
#define RST_LOW             RST_PORT->BSRR=(1<<(RST_PIN+16))
/// Clock
#define CLK_PORT            GPIOC
#define CLK_PIN             7
#define CLK_INPUT           CLK_PORT->CRL|=(4<<(4*(CLK_PIN)))
#define CLK_OUTPUT          CLK_PORT->CRL&=~(6<<(4*(CLK_PIN)))
#define CLK_HIGH            CLK_PORT->BSRR=(1<<CLK_PIN)
#define CLK_LOW             CLK_PORT->BSRR=(1<<(CLK_PIN+16))
/// Data
#define DIO_PORT            GPIOC
#define DIO_PIN             6
#define DIO_INPUT           DIO_PORT->CRL|=(4<<(4*(DIO_PIN)))
#define DIO_OUTPUT          DIO_PORT->CRL&=~(6<<(4*(DIO_PIN)))
#define DIO_HIGH            DIO_PORT->BSRR=(1<<DIO_PIN)
#define DIO_LOW             DIO_PORT->BSRR=(1<<(DIO_PIN+16))
#define DIO_DATA            DIO_PORT->IDR&(1<<DIO_PIN)
/// AUX
#define AUX_PORT            GPIOC
#define AUX_PIN             5
#define AUX_INPUT           AUX_PORT->CRL|=(4<<(4*(AUX_PIN)))
#define AUX_OUTPUT          AUX_PORT->CRL&=~(6<<(4*(AUX_PIN)))
#define AUX_HIGH            AUX_PORT->BSRR=(1<<AUX_PIN)
#define AUX_LOW             AUX_PORT->BSRR=(1<<(AUX_PIN+16))
#define AUX_DATA            AUX_PORT->IDR&(1<<AUX_PIN)
//

#define DATABUFSIZE (256)
struct FIFO_t
{
    uint8_t buf[DATABUFSIZE];
    uint16_t Put;
    uint16_t Get;
};

#define USART2_ENABLE  USART2->CR1 |= USART_CR1_UE
#define USART2_DISABLE USART2->CR1 &= ~USART_CR1_UE
#define USART2_EMPTY  (USART2->SR & USART_SR_TC)
#define USART2_SEND(CH) ( USART2->DR = (CH) )


// CDC definitions
#define CDC_CHAR_FORMAT_1_STOP_BIT     0
#define CDC_CHAR_FORMAT_1_5_STOP_BIT   1
#define CDC_CHAR_FORMAT_2_STOP_BIT     2

#define CDC_PARITY_TYPE_NONE           0
#define CDC_PARITY_TYPE_ODD            1
#define CDC_PARITY_TYPE_EVEN           2
#define CDC_PARITY_TYPE_MARK           3
#define CDC_PARITY_TYPE_SPACE          4
struct cdcLineCoding_t
{
    uint32_t dwDTERate;       /// Data terminal rate {115200,0,0,8,2,}
    uint8_t  bStopBits;       /// Number of stop bits
    uint8_t  bParityType;     /// Parity bit type
    uint8_t  bDataBits;       /// Number of data bits
//    uint8_t  bState;
};
/// Where
/// dwDTERate	Baudrate in bits per second.
/// bCharFormat	Number of stop bits (0 = 1 stop bit, 1 = 1.5 stop bits, 2 = 2 stop bits).
/// bParityType	Parity type (0 = None, 1 = Odd, 2 = Even, 3 = Mark, 4 = Space).
/// bDataBits	Number of data bits (5, 6, 7, 8, or 16).
/// bState Serial State     bit 0: DTR state
///                         bit 1: RTS state
//
extern uint32_t BoardTicks,BoardSeconds;
extern uint16_t BoardFlags,rtcAlarm,LedTimer;
extern uint8_t RedLedMask,LedMask;
extern struct FIFO_t OUT_FIFO,IN_FIFO;
extern uint8_t uart1CMD,uart2CMD;
extern struct LineCoding_t uartCDC;

#define _RTC_TDIF	+6.0	/* OST = UTC+6.0 */

struct RTCTIME_t
{
    uint16_t	year;	/* 1970..2106 */
    uint8_t		month;	/* 1..12 */
    uint8_t		mday;	/* 1..31 */
    uint8_t		hour;	/* 0..23 */
    uint8_t		min;	/* 0..59 */
    uint8_t		sec;	/* 0..59 */
    uint8_t		wday;	/* 0..6 (Sun..Sat) */
};

void rtc_gettime (struct RTCTIME_t* rtc);			/* Get time */
void rtc_settime (struct RTCTIME_t* rtc);	/* Set time */
extern struct RTCTIME_t rtcTIME;
//
void SystemInit (void);
void SystemClock_Config(void);
void initBoard(void);

#define USART1_ENABLE USART1->CR1 |= USART_CR1_UE
#define USART2_ENABLE USART2->CR1 |= USART_CR1_UE
//void uart1_putc(uint8_t ch);
//uint8_t uart1_getc(void);
void uart2_putc(uint8_t ch);
uint8_t uart2_getc(void);
void outCalendar(void);
void CheckButton(void);
void printHelp(void);
void DelayAlarm(uint16_t ticks);
void UsbUart(void);
uint8_t outDS18B20(void);
uint8_t getDigit(long *digit);
void hexprtBuf(uint8_t *buf,uint16_t length);
void usbISR(void);
void prtUart(void);

#endif // BOARD_H_INCLUDED
