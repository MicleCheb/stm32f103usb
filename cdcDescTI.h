#ifndef CDCDESC_H_INCLUDED
#define CDCDESC_H_INCLUDED
//#include <string.h>
#include <stdint.h>

#define USB_NONE_EP         -1
#define USB_CONTROL_EP      0
#define USB_INT_EP          1
#define USB_CDC_EP          1
#define USB_OUT_EP          2
#define USB_IN_EP           3
#define USB_CONTROL_SIZE    32
#define EP_DOUBLE_BUFFER    0
#define USB_CDC_SIZE        16
#define USB_DATA_SIZE       32
#define USB_IN_SIZE         (USB_DATA_SIZE)
#define USB_OUT_SIZE        (USB_DATA_SIZE)

#define LE(X) ((uint16_t)(X)&0x00FF),((uint16_t)(X)>>8)
const uint8_t cdcDescriptor[]=
{
//Device Descriptor:
//------------------------------
    0x12,//	bLength
    0x01,//	bDescriptorType
    LE(0x0110),//	bcdUSB
    0x00,//	bDeviceClass   (Communication Device Class)
    0x00,//	bDeviceSubClass
    0x00,//	bDeviceProtocol
    USB_CONTROL_SIZE,//	bMaxPacketSize0   (32 Bytes)
    LE(0x0451),//	idVendor03EB&PID_2044
    LE(0x16a4),
//    LE(0xF432),//	idProduct 16A4 0xF432
    LE(0x0100),//	bcdDevice
    0x01,//	iManufacturer
    0x02,//	iProduct
    0x03,//	iSerialNumber
    0x01,//	bNumConfigurations

//Configuration Descriptor:
//------------------------------
    0x09,//	bLength
    0x02,//	bDescriptorType
    LE(67-9-5),//	wTotalLength -9-5
    0x01,//	bNumInterfaces
    0x01,//	bConfigurationValue
    0x00,//	iConfiguration
    0x80,//	bmAttributes   (Bus-powered Device)
    0x32,//	bMaxPower   (100 mA)

//Interface Descriptor:
//------------------------------
    0x09,//	bLength
    0x04,//	bDescriptorType
    0x00,//	bInterfaceNumber
    0x00,//	bAlternateSetting
    0x03,//	bNumEndPoints
    0x02,//	bInterfaceClass   (Communication Device Class)
    0x02,//	bInterfaceSubClass   (Abstract Control Model)
    0x02,//	bInterfaceProtocol   (ITU-T V.250)
    0x04,//	iInterface

//CDC Header Functional Descriptor:
//------------------------------
    0x05,//	bFunctionalLength
    0x24,//	bDescriptorType
    0x00,//	bDescriptorSubtype
    LE(0x0110),//	bcdCDC

//CDC Call Management Functional Descriptor:
//------------------------------
    0x05,//	bFunctionalLength
    0x24,//	bDescriptorType
    0x01,//	bDescriptorSubtype
//0x03,//	bmCapabilities
    0x00,// bmCapabilities Device does not handle call management itself.
    0x00+0,//	bDataInterface

//CDC Abstract Control Management Functional Descriptor:
//------------------------------
    0x04,//	bFunctionalLength
    0x24,//	bDescriptorType
    0x02,//	bDescriptorSubtype
    0x02,   // bmCapabilities
//  b1: 1 - Device supports the request combination of Set_Line_Coding,
//  Set_Control_Line_State, Get_Line_Coding, and the notification Serial_State.
//CDC Union Functional Descriptor:
//------------------------------
//    0x05,//	bFunctionalLength
//    0x24,//	bDescriptorType
//    0x06,//	bDescriptorSubtype
//    0x00,//	bControlInterface
//    0x01,//	bSubordinateInterface(0)

//Endpoint Descriptor:
//------------------------------
    0x07,//	bLength
    0x05,//	bDescriptorType
    0x80+USB_INT_EP,//	bEndpointAddress   (IN Endpoint)
    0x03,//	bmAttributes	(Transfer: Interrupt / Synch: None / Usage: Data)
    LE(USB_CDC_SIZE),//	wMaxPacketSize   (16 Bytes)
    0xff,//	bInterval

//Interface Descriptor:
//------------------------------
//    0x09,//	bLength
//    0x04,//	bDescriptorType
//    0x01,//	bInterfaceNumber
//    0x00,//	bAlternateSetting
//    0x02,//	bNumEndPoints
//    0x0A,//	bInterfaceClass   (CDC Data)
//    0x00,//	bInterfaceSubClass
//    0x00,//	bInterfaceProtocol
//    0x00,//	iInterface

//Endpoint Descriptor:
//------------------------------
    0x07,//	bLength
    0x05,//	bDescriptorType
    0x80+USB_IN_EP,//	bEndpointAddress   (IN Endpoint)
    0x02,//	bmAttributes	(Transfer: Bulk / Synch: None / Usage: Data)
    LE(USB_IN_SIZE),//	wMaxPacketSize   (64 Bytes)
    0xff,//	bInterval

//Endpoint Descriptor:
    0x07,//	bLength
    0x05,//	bDescriptorType
    USB_OUT_EP,//	bEndpointAddress   (OUT Endpoint)
    0x02,//	bmAttributes	(Transfer: Bulk / Synch: None / Usage: Data)
    LE(USB_OUT_SIZE),//	wMaxPacketSize   (64 Bytes)
    0xff,//	bInterval
//------------------------------
//String Descriptor Table
// String index0, language support
    4,        // Length of language descriptor ID
    3,        // LANGID tag
    LE(0x0409),    // 0x0409 for English
// String index1, Manufacturer
    36,        // Length of this string descriptor
    3,        // bDescriptorType
    'T',0x00,'e',0x00,'x',0x00,'a',0x00,'s',0x00,' ',0x00,
    'I',0x00,'n',0x00,'s',0x00,'t',0x00,'r',0x00,'u',0x00,
    'm',0x00,'e',0x00,'n',0x00,'t',0x00,'s',0x00,
// String index2, Product
    36,        // Length of this string descriptor
    3,        // bDescriptorType
    'T',0x00,'I',0x00,' ',0x00,'U',0x00,'S',0x00,'B',0x00,
    ' ',0x00,'C',0x00,'D',0x00,'C',0x00,' ',0x00,'D',0x00,
    'r',0x00,'i',0x00,'v',0x00,'e',0x00,'r',0x00,

// String index3, Serial Number
    6,        // Length of this string descriptor
    3,        // bDescriptorType
    'A',0x00,'3',0x00,
// String index4, Interface
    60,        // Length of this string descriptor
    3,        // bDescriptorType
    'T',0x00,'I',0x00,' ',0x00,'C',0x00,'C',0x00,'2',0x00,
    '5',0x00,'1',0x00,'1',0x00,' ',0x00,'U',0x00,'S',0x00,
    'B',0x00,' ',0x00,'C',0x00,'D',0x00,'C',0x00,' ',0x00,
    'S',0x00,'e',0x00,'r',0x00,'i',0x00,'a',0x00,'l',0x00,
    ' ',0x00,'P',0x00,'o',0x00,'r',0x00,'t',0x00,
0,
};

#endif // CDCDESC_H_INCLUDED
